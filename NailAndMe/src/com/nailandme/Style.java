package com.nailandme;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class Style extends BaseActivity {


	void initButtonAction()
	{
		Button buttonMenu;		
		
		buttonMenu = (Button) findViewById(R.id.button_menu);		
		buttonMenu.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if( linearlayoutMenu.getVisibility() == View.VISIBLE )
					linearlayoutMenu.setVisibility( View.INVISIBLE );
				else
					linearlayoutMenu.setVisibility( View.VISIBLE );
			}
		});
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    	
	    setContentView(R.layout.style);
	    
	    setMenuLayout();
	    setMenuButtonActions( linearlayoutMenu );
	    
	    initButtonAction();
	    
	}

}
