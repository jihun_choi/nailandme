package com.nailandme;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Trend extends BaseActivity {
	
	WebView webViewNews;
	WebView webViewWhatsHot;

	
	void initButtonAction()
	{
		Button buttonMenu;
		Button buttonNews;
		Button buttonWhatsHot;
		
		buttonMenu = (Button) findViewById(R.id.button_menu);		
		buttonMenu.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if( linearlayoutMenu.getVisibility() == View.VISIBLE )
					linearlayoutMenu.setVisibility( View.INVISIBLE );
				else
					linearlayoutMenu.setVisibility( View.VISIBLE );
			}
		});
		
		buttonNews = (Button) findViewById(R.id.button_news);		
		buttonNews.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				webViewNews.setVisibility( View.VISIBLE );
				webViewWhatsHot.setVisibility( View.INVISIBLE );
			}
		});
		
		buttonWhatsHot = (Button) findViewById(R.id.button_whats_hot);
		buttonWhatsHot.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				webViewNews.setVisibility( View.INVISIBLE );
				webViewWhatsHot.setVisibility( View.VISIBLE );
			}
		});
	}
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    
	    setContentView(R.layout.trend);
	    
	    setMenuLayout();
	    setMenuButtonActions( linearlayoutMenu );
	    
	    webViewNews = (WebView) findViewById(R.id.webview_news); 
	    webViewNews.getSettings().setJavaScriptEnabled(true);  // 웹뷰에서 자바스크립트실행가능
	    webViewNews.loadUrl("http://www.google.com");  // 구글홈페이지 지정
	    webViewNews.setWebViewClient(new HelloWebViewClient());  // WebViewClient 지정
	    
	    webViewWhatsHot = (WebView) findViewById(R.id.webview_whats_hot); 
	    webViewWhatsHot.getSettings().setJavaScriptEnabled(true);  // 웹뷰에서 자바스크립트실행가능
	    webViewWhatsHot.loadUrl("http://www.naver.com");  // 구글홈페이지 지정
	    webViewWhatsHot.setWebViewClient(new HelloWebViewClient());  // WebViewClient 지정
	    
	    initButtonAction();
	}
	
	 private class HelloWebViewClient extends WebViewClient { 
		 @Override 
		 public boolean shouldOverrideUrlLoading(WebView view, String url) { 
			 view.loadUrl(url); 
			 return true; 
		 }
    } 
}
