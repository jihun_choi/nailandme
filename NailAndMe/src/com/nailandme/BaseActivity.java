package com.nailandme;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.nailandme.util.DialogUtil;

public class BaseActivity extends Activity {
	
	Context mContext;
	
	LayoutInflater inflater;
	
	FrameLayout framelayoutContent;
	LinearLayout linearlayoutMenu;
	
	Button buttonTrend;
	Button buttonProducts;
	Button buttonStyle;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    
	    mContext = this;
	}
	
	void setMenuLayout()
	{
		ViewGroup view = (ViewGroup)getWindow().getDecorView();
		
		framelayoutContent = (FrameLayout) view.getChildAt(0);
	    inflater = (LayoutInflater)this.getLayoutInflater();
	    
	    linearlayoutMenu = (LinearLayout) inflater.inflate(R.layout.menu, null);
	    framelayoutContent.addView(linearlayoutMenu, 1);
	}
		
	public void setMenuButtonActions( LinearLayout linearlayoutMenu )
	{
		buttonTrend = (Button) linearlayoutMenu.getChildAt(0);
		buttonTrend.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {				
				Intent intent = new Intent( mContext, Trend.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
				startActivity(intent);				
			}
		});
		
		buttonProducts = (Button) linearlayoutMenu.getChildAt(1);
		buttonProducts.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {				
				Intent intent = new Intent( mContext, Products.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
				startActivity(intent);				
			}
		});
		
		buttonStyle = (Button) linearlayoutMenu.getChildAt(2);
		buttonStyle.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {				
				Intent intent = new Intent( mContext, Style.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
				startActivity(intent);				
			}
		});
	}
	
	 public void confirmFinish()
	 {
		 DialogUtil.confirm( mContext, R.string.app_finish, new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{	
					moveTaskToBack(true);
					finish();
					android.os.Process.killProcess(android.os.Process.myPid() ); 
				}
			}).show();	
	 }
	 
	 @Override
	 public void onBackPressed() 
	 {
		 confirmFinish();
		 return ;
	 }

}
