package com.nailandme;

import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Button;

public class MainBeforeLogin extends BaseActivity {

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	
	    // TODO Auto-generated method stub
	    setContentView( R.layout.main_before_login );
	    
	    Button buttonSignIn = (Button) findViewById( R.id.button_sign_in );
	    
	    buttonSignIn.setOnTouchListener( new OnTouchListener() {
			
			public boolean onTouch(View v, MotionEvent event) {
				
				if( event.getAction() == event.ACTION_UP )
				{
					Intent intent = new Intent( mContext, Trend.class);
					intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
					startActivity(intent);
				}
				
				return false;
			}
		});
	}
}
