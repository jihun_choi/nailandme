package com.nailandme.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.nailandme.R;

public class DialogUtil {
	
	static public AlertDialog alert(Context ctx, String msg) {
		AlertDialog alert = new AlertDialog.Builder(ctx)
			.setMessage(msg)
			.setPositiveButton(ctx.getString(R.string.text_confirm), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			})
			.create();
		return alert;
	}
	
	static public AlertDialog alert(Context ctx, int msg) {
		AlertDialog alert = new AlertDialog.Builder(ctx)
			.setMessage(msg)
			.setPositiveButton(ctx.getString(R.string.text_confirm), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			})
			.create();
		return alert;
	}
	
	static public AlertDialog alert(Context ctx, int msg, DialogInterface.OnClickListener click) {
		AlertDialog alert = new AlertDialog.Builder(ctx)
			.setMessage(msg)
			.setPositiveButton(ctx.getString(R.string.text_confirm), click)
			.create();
		return alert;
	}
	
	static public AlertDialog alert(Context ctx, String title, String msg) {
		AlertDialog alert = new AlertDialog.Builder(ctx)
			.setTitle(title)
			.setMessage(msg)
			.setPositiveButton(ctx.getString(R.string.text_confirm), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			})
			.create();
		return alert;
	}
	
	static public AlertDialog confirm(Context ctx, String msg, DialogInterface.OnClickListener yesClick) {
		AlertDialog alert = new AlertDialog.Builder(ctx)
			.setMessage(msg)
			.setPositiveButton(ctx.getString(R.string.yes), yesClick)
			.setNegativeButton(ctx.getString(R.string.no), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			})
			.create();
		return alert;
	}
	
	static public AlertDialog confirm(Context ctx, int msg, DialogInterface.OnClickListener yesClick) {
		AlertDialog alert = new AlertDialog.Builder(ctx)
			.setMessage(msg)
			.setPositiveButton(ctx.getString(R.string.yes), yesClick)
			.setNegativeButton(ctx.getString(R.string.no), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			})
			.create();
		return alert;
	}
	
	static public AlertDialog confirm(Context ctx, String title, String msg, DialogInterface.OnClickListener yesClick) {
		AlertDialog alert = new AlertDialog.Builder(ctx)
			.setTitle(title)
			.setMessage(msg)
			.setPositiveButton(ctx.getString(R.string.yes), yesClick)
			.setNegativeButton(ctx.getString(R.string.no), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			})
			.create();
		return alert;
	}
	
	static public AlertDialog confirm(Context ctx, int title, int msg, DialogInterface.OnClickListener yesClick) {
		AlertDialog alert = new AlertDialog.Builder(ctx)
			.setTitle(title)
			.setMessage(msg)
			.setPositiveButton(ctx.getString(R.string.yes), yesClick)
			.setNegativeButton(ctx.getString(R.string.no), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			})
			.create();
		return alert;
	}
	
	/**
	 * 공지사항 팝업 - 메인메뉴 진입후 처리 
	 * @param ctx
	 * @param title
	 * @param msg
	 * @param yesClick
	 * @return
	 */
	
	/*
	static public AlertDialog alert_notice(Context ctx, String title, String msg, DialogInterface.OnClickListener yesClick) {
		
		final LinearLayout linear = (LinearLayout) View.inflate(ctx,R.layout.alert_notice, null);
		final SharedPreferences preference = PreferenceManager.getDefaultSharedPreferences(ctx);
		
		// 1. 공지사항 그만보기를 이미 처리 했는지 확인 한뒤에 팝업을 뛰운다. 
		// 2. 
		// 3. 
		
		
		AlertDialog alert = new AlertDialog.Builder(ctx)
			.setIcon(R.drawable.app_icon)
			.setTitle(title)
			.setView(linear)
			.setMessage(msg)
			.setPositiveButton(ctx.getString(R.string.text_confirm), new DialogInterface.OnClickListener() { 
				@Override
				public void onClick(DialogInterface dialog, int which) {
					 
					preference.getBoolean("auto_login", false);
					dialog.dismiss();
				}
			})
			.create();
		return alert;
	}*/
}
