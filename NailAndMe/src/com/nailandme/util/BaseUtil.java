package com.nailandme.util;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

public class BaseUtil 
{
	public static String convertStreamToString(InputStream is) 
	{
	    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
	    StringBuilder sb = new StringBuilder();
	
	    String line = null;
	    try {
	        while ((line = reader.readLine()) != null) {
	            sb.append(line + "\n");
	        }
	    } catch (IOException e) {
	        e.printStackTrace();
	    } finally {
	        try {
	            is.close();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	    }
	    return sb.toString();
    }
	
	
	public static Bitmap getImageBitmap(String url) 
	{ 
         Bitmap bm = null; 
         try { 
             URL aURL = new URL(url); 
             URLConnection conn = aURL.openConnection(); 
             conn.connect(); 
             InputStream is = conn.getInputStream(); 
             BufferedInputStream bis = new BufferedInputStream(is); 
             bm = BitmapFactory.decodeStream(bis); 
             bis.close(); 
             is.close(); 
        } catch (IOException e) { 
            Log.e("1", "Error getting bitmap", e); 
        } 
        return bm; 
     }
}
